package carms.template.dto;

public class TemplateDTO {

public TemplateDTO(){ /*Implicit private constructor*/ }

    Long id;
    String templateColumn;


    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getTemplateColumn() {
        return templateColumn;
    }
    public void setTemplateColumn(String templateColumn) {
        this.templateColumn = templateColumn;
    }
}
