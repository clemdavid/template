package carms.template.mapper;

import carms.template.dto.TemplateDTO;
import carms.template.entity.EntityTemplate;

public final class TemplateMapper {

    private TemplateMapper(){ /* Implicit constructor */}

    public static EntityTemplate convertToEntity(TemplateDTO dto){
        EntityTemplate entity = new EntityTemplate();
        entity.setId(dto.getId());
        entity.setTemplateColumn(dto.getTemplateColumn());
        return entity;
    }

    public static TemplateDTO convertToDTO(EntityTemplate entity){
        TemplateDTO dto = new TemplateDTO();
        dto.setId(entity.getId());
        dto.setTemplateColumn(entity.getTemplateColumn());
        return dto;
    }
}
