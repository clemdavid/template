package carms.template.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class EntityTemplate {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, insertable = true, updatable = true, name = "templateColumn", unique = true, length = 20 )
    private String templateColumn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getTemplateColumn() {
        return templateColumn;
    }

    public void setTemplateColumn(String templateColumn) {
        this.templateColumn = templateColumn;
    }
}
