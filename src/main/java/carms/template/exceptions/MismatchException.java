package carms.template.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class MismatchException extends RuntimeException {

    public MismatchException(){
        super();
    }

    public MismatchException(String message, Throwable cause){
        super(message, cause);
    }
}
