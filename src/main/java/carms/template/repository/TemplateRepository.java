package carms.template.repository;

import org.springframework.data.repository.CrudRepository;

import carms.template.entity.EntityTemplate;

public interface TemplateRepository extends CrudRepository<EntityTemplate, Long>{
    EntityTemplate findByTemplateColumn(String templateColumn);
}
