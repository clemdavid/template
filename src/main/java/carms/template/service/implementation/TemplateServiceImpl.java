package carms.template.service.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import carms.template.dto.TemplateDTO;
import carms.template.entity.EntityTemplate;
import carms.template.mapper.TemplateMapper;
import carms.template.repository.TemplateRepository;
import carms.template.service.TemplateService;

public class TemplateServiceImpl implements TemplateService {

    @Autowired
    TemplateRepository templateRepository;

    @Override
    public List<TemplateDTO> findall() {
        List<TemplateDTO> list = new ArrayList<>();
        templateRepository.findAll().forEach(r -> {
            list.add(TemplateMapper.convertToDTO(r));
        });
        return list;
    }

    @Override
    public TemplateDTO findOne(Long id) {
        Optional<EntityTemplate> opt = templateRepository.findById(id);
        return opt.isPresent() ? TemplateMapper.convertToDTO(opt.get()) : new TemplateDTO();
    }
    
}
