package carms.template.service;

import java.util.List;

import org.springframework.stereotype.Service;

import carms.template.dto.TemplateDTO;

@Service
public interface TemplateService {
    public List<TemplateDTO> findall();
    public TemplateDTO findOne(Long id);
}
