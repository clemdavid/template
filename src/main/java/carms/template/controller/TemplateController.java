package carms.template.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import carms.template.dto.TemplateDTO;
import carms.template.entity.EntityTemplate;
import carms.template.exceptions.MismatchException;
import carms.template.exceptions.NotFoundException;
import carms.template.mapper.TemplateMapper;
import carms.template.repository.TemplateRepository;
import carms.template.service.TemplateService;
import carms.template.service.implementation.TemplateServiceImpl;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

@Controller
public class TemplateController {

    @Autowired
    TemplateRepository templateRepository;

    TemplateService templateService = new TemplateServiceImpl();

    @Value("${spring.application.name}")
    String appName;

    @GetMapping("/")
    public String homePage(Model model) {
        model.addAttribute("appName", appName);
        return "home";
    }

    /**
     * return all rows from table
     * @return a collection of objects
     */
    @GetMapping("all/")
    public List<TemplateDTO> findAll() {
        return templateService.findall();
    }

    /**
     * return an object from given field
     * @param templateColumn the id to get
     * @return an object from database
     */
    @GetMapping("/column/{templateColumn}")
    public EntityTemplate findByTitle(@PathVariable String templateColumn) {
        return templateRepository.findByTemplateColumn(templateColumn);
    }

    /**
     * return an object from it's ID
     * @param id the id of object to return
     * @return an object from database
     */
    @GetMapping("/{id}")
    public TemplateDTO findOne(@PathVariable Long id) {
        return templateService.findOne(id);
    }

    /**
     * insert a new object in database
     * @param template the object to insert
     * @return an httpstatus code and the object inserted
     */
    @PostMapping("create/")
    @ResponseStatus(code = HttpStatus.CREATED)
    public EntityTemplate create(@RequestBody TemplateDTO template) {
        return templateRepository.save(TemplateMapper.convertToEntity(template));
    }

    /**
     * delete the object in database
     * @param id the object to delete defined from it's ID
     */
    @DeleteMapping("delete/{id}")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id) {
        try{
            templateRepository.findById(id);
            templateRepository.deleteById(id);
        }catch(NotFoundException nfe){
            throw nfe;
        }
    }

    /**
     * Update the given object from database
     * @param template the object to update in database
     * @param id the id of the object to update
     * @return an object updated
     */
    @PutMapping("update/{id}")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public EntityTemplate updateBook(
        @RequestBody TemplateDTO template, 
        @PathVariable Long id) {
        
            if(Objects.isNull(template)){
                throw new IllegalArgumentException();
            }

            if(!template.getId().equals(id)){
                throw new MismatchException();
            }

            templateRepository.findById(id);
            return templateRepository.save(TemplateMapper.convertToEntity(template));
    }
}
